import string

class KITPasswordChecker:

    Checks = {
        'Lowercase': { 'result':  1, 'checker': 'check_lowercase', 'explanation': 'does not contain lowercase characters' },
        'Uppercase': { 'result':  2, 'checker': 'check_uppercase', 'explanation': 'does not contain uppercase characters' },
        'Digits':    { 'result':  4, 'checker': 'check_digits',    'explanation': 'does not contain digits' },
        'Specials':  { 'result':  8, 'checker': 'check_specials',  'explanation': 'does not contain special characters' },
        'Length':    { 'result': 16, 'checker': 'check_length',    'explanation': 'is not long enough' },
    }

    uppercase  = string.ascii_uppercase
    lowercase  = string.ascii_lowercase
    digits     = string.digits
    specials   = list('!"$%&\'()*+,-./:;<=>?[\]^_`{|}~ ')
    min_length = 8
    
    @staticmethod
    def contains_any(str, set):
        """Check whether 'str' contains ANY of the chars in 'set'"""
        return KITPasswordChecker.contains_n_of(str, set, 1)
    
    @staticmethod
    def contains_n_of(str, set, n):
        """Check whether 'str' contains at least n of the chars in 'set'"""
        return ([c in str for c in set].count(True) >= n)

    @staticmethod
    def check(password):
        """Check a password against the general KIT password rules."""
        result = 0
        for check in KITPasswordChecker.Checks:
            if not getattr(KITPasswordChecker, KITPasswordChecker.Checks[check]['checker'])(password):
                result |= KITPasswordChecker.Checks[check]['result']
        return(result)

    @staticmethod
    def check_verbose(password):
        result = KITPasswordChecker.check(password)
        return(', '.join(KITPasswordChecker.explain_result(result)))

    @staticmethod
    def explain_result(result):
        explanation = []
        for check in KITPasswordChecker.Checks:
            if (result & KITPasswordChecker.Checks[check]['result'] > 0):
                explanation.append(KITPasswordChecker.Checks[check]['explanation'])
        return(explanation)

    @staticmethod
    def check_uppercase(password):
        return (KITPasswordChecker.contains_any(password, KITPasswordChecker.uppercase))

    @staticmethod
    def check_lowercase(password):
        return (KITPasswordChecker.contains_any(password, KITPasswordChecker.lowercase))

    @staticmethod
    def check_digits(password):
        return (KITPasswordChecker.contains_any(password, KITPasswordChecker.digits))

    @staticmethod
    def check_specials(password):
        return (KITPasswordChecker.contains_any(password, KITPasswordChecker.specials))

    @staticmethod
    def check_length(password):
        return (len(password) >= KITPasswordChecker.min_length)


class SCCPasswordChecker(KITPasswordChecker):

    Checks = {
        'Braces':    { 'result':  32, 'checker': 'check_braces',   'explanation': 'is enclosed in braces' },
        'Specials':  { 'result':  64, 'checker': 'check_specials', 'explanation': 'does not contain two different special characters' },
        'Length':    { 'result': 128, 'checker': 'check_length',   'explanation': 'is not long enough (SCC limit)' },
    }

    min_length = 12

    @staticmethod
    def check(password):
        """Check a password against the SCC password rules (on top of the
        KIT rules)."""
        result = KITPasswordChecker.check(password)
        for check in SCCPasswordChecker.Checks:
            if not getattr(SCCPasswordChecker, SCCPasswordChecker.Checks[check]['checker'])(password):
                result |= SCCPasswordChecker.Checks[check]['result']
        return(result)

    @staticmethod
    def check_verbose(password):
        result = SCCPasswordChecker.check(password)
        return(', '.join(SCCPasswordChecker.explain_result(result)))

    @staticmethod
    def explain_result(result):
        explanation = KITPasswordChecker.explain_result(result)
        for check in SCCPasswordChecker.Checks:
            if (result & SCCPasswordChecker.Checks[check]['result'] > 0):
                explanation.append(SCCPasswordChecker.Checks[check]['explanation'])
        return(explanation)

    @staticmethod
    def check_braces(password):
        return not ((password.startswith('(') and password.endswith(')')) or
                    (password.startswith('<') and password.endswith('>')) or
                    (password.startswith('[') and password.endswith(']')) or
                    (password.startswith('{') and password.endswith('}')))

    @staticmethod
    def check_specials(password):
        return (KITPasswordChecker.contains_n_of(password, KITPasswordChecker.specials, 2))

    @staticmethod
    def check_length(password):
        return (len(password) >= SCCPasswordChecker.min_length)
