# Password Generator

The password generator is available as stand-alone Windows 32-bit executable,
which can be downloaded from [the release notes](https://git.scc.kit.edu/KIT-CERT/passwordGenerator/tags).

If you want to roll the Windows EXE file yourself, you will need a Windows
system with a working Python 3 installation and the `pyinstaller` package
installed.  Running `Windows\roll_executable.bat` should create a working binary
in `dist\passwordGenerator.exe`.


## Entropy Discussion

In the discussion below, we shall assume that only printable ASCII
characters are used in passwords.

Under the KIT password rules, a minimum-length password has 8 characters.
Given the fact that it MUST include at least one upper-case letter, one
lower-case letter, one digit, and one of 31 special characters, the number
of possible combinations of such a password are:

    26 * 26 * 10 * 31 * 93^4 = 15676177921560

That is equivalent of an entropy of 43.83 bit.

Under the SCC password rules, the minimum length is 12, but there is an
additional rule demanding that two separate special characters must be
used.  Furthermore, bracketing a password within a set of braces, brackets,
parentheses, or angular brackets is not allowed.  Thus, there are fewer
combinations than

    26 * 26 * 10 * 31 * 30 * 93^7 = 378277303333567107600

This yields an entropy of slightly less than 68.36 bit.

This tool picks words from a set of 4096 words, capitalizing the first
letter at random.  Thus, every word that is randomly picked adds 13 bits of
entropy.  After each word, a separator character is chosen at random from a
set of either 16 or 40 characters, depending on the settings.  Thus, every
separator adds another 4 or a little more than 5 bits of entropy (for this
discussion, we assume that it is 5 bits; this makes calculation easier and
errs on the side of caution).  Therefore, in order to get a password that
is as strong as a KIT-rule-based completely random password, one needs to
come up with at least 44 bits of entropy.  This can easily done by chosing
a

    word - separator - word - separator - word

combination (for 13 + 4 + 13 + 4 + 13 = 47 bits under the limited set of
separator characters or 13 + 5 + 13 + 5 + 13 = 49 bits under the extended
set of separator characters).

For an SCC-rule compliant password, it is necessary to come up with at
least 69 bits of entropy; this can be done by picking a sequence of

    word - separator - word - separator - word - separator - word - separator - word

combination, yielding 13 + 4 + 13 + 4 + 13 + 4 + 13 + 4 + 13 = 81 bits
using the limited separator set.  If the extended separator set is used,
the combination

    word - separator - word - separator - word - separator - word + separator

is sufficient as it gives 13 + 5 + 13 + 5 + 13 + 5 + 13 + 5 = 82 bits of
entropy.

In general, as a rough estimate, every entirely random character in a
password consisting of characters of the above sets yields about 6.54 bits
of entropy, so a word-separator combination is about equivalent to 2.5
characters (or even 2.75 characters if the extended separator set is used).

The maximum entropy is set to 120 bits because actually anything much more than
80 bits does not really contribute much in the way of security if the entropy is
properly generated (i. e., actually random).  120 bits adds some margin of error
and should be secure.
