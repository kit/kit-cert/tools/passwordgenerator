import random
from math import log
from wordset import words

class PasswordGenerator:

    min_word_length = min(len(word) for word in words)
    # The min word length is 3 characters.

    max_word_length = max(len(word) for word in words)
    # The max word length is 6 characters.

    avg_word_length = 1.0*sum(len(word) for word in words)/len(words)
    # The avg word length is 4.98

    word_entropy = int(log(len(words), 2)) + 1
    # There are 4096 words, and the first character of every word can be
    # either uppercase or lowercase, so every use of a word token carries
    # an entropy of log_2(4096) + 1 = 13 bits.

    separators = {
        False: '!$&*+-=_23456789',
        True:  '!"$%&\'()*+,-./:;<=>?[\]^_`{|}~1234567890'
    }
    separator_entropy = {
        False: int(log(len(separators[False]), 2)),
        True:  int(log(len(separators[True]),  2))
    }
    # There are 40 separators, so every use of a separator token carries an
    # entropy of log_2(40) > 5 bits. 

    def __init__(self):
        """Create a new PasswordGenerator object."""
        try:
            self.rnd = random.SystemRandom()
        except:
            self.rnd = random.Random()

    def get_token_count(self, bits, use_extended=False):
        # Observe the following facts:
        # 1. target_entropy = word_count * word_entropy + separator_count * separator_entropy + rest
        # 2. There can never be more separators than words -> word_count = separator_count + {0, 1}
        # 3. If target_entropy is divisible by (word_entropy + separator_entropy), then both counts are equal to the quotient
        # 4. Otherwise, if the rest is larger than word_entropy, both counts are equal to the quotient + 1;
        #    if the rest is smaller than or equal to word_entropy, only the word_count is equal to the quotient + 1
        word_count = int(bits / (PasswordGenerator.word_entropy + PasswordGenerator.separator_entropy[use_extended]))
        separator_count = word_count
        rest = bits - (word_count * (PasswordGenerator.word_entropy + PasswordGenerator.separator_entropy[use_extended]))
        if (rest > 0):
            word_count += 1
            if (rest > PasswordGenerator.word_entropy):
                separator_count += 1
        return(word_count, separator_count)

    def get_min_length(self, bits, use_extended=False):
        # Work out how long our password will be at minimum
        (word_count, separator_count) = self.get_token_count(bits, use_extended)
        return((word_count * PasswordGenerator.min_word_length) + separator_count)
        
    def get_max_length(self, bits, use_extended=False):
        # Work out how long our password will be at maximum
        (word_count, separator_count) = self.get_token_count(bits, use_extended)
        return((word_count * PasswordGenerator.max_word_length) + separator_count)
        
    def estimate_length(self, bits, use_extended=False):
        # Work out how long our password will be on average
        (word_count, separator_count) = self.get_token_count(bits, use_extended)
        return(int(round(word_count * PasswordGenerator.avg_word_length) + separator_count))
        
        
    def generate(self, randomBits=47, use_extended=False, maxLength=None):
        """Generate a random password containing at least randomBits of
        information, optionally with a maximum length."""
        
        # Restrict the number of random bits to a reasonable range
        bits = randomBits
        if bits < 26:
            bits = 26

        # Work out how long our password will be at most
        length = self.get_max_length(randomBits, use_extended)

        # If it's too long, exit
        if maxLength and length > maxLength:
            return None

        output = []
        length = 0
        while bits > 0:
            wordNdx = self.rnd.randint(0, len(words)-1)
            
            word = words[wordNdx]
            if (self.rnd.randint(0, 1)):
                word = word.capitalize()

            output.append(word)

            length += len(word)
            bits -= PasswordGenerator.word_entropy

            if bits > 0:
                sepNdx = self.rnd.randint(0, len(self.separators[use_extended])-1)
                output.append(self.separators[use_extended][sepNdx])
                bits -= PasswordGenerator.separator_entropy[use_extended]

        return ''.join(output)

                
