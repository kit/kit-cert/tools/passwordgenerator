#! /usr/bin/env python
# -*- coding: utf-8 -*-

try:
    # for Python2
    from Tkinter import *   ## notice capitalized T in Tkinter
except ImportError:
    # for Python3
    from tkinter import *   ## notice here too
from math import log
from pwgen import PasswordGenerator
from kitpwcheck import KITPasswordChecker, SCCPasswordChecker

class password_generator:
    per_char_entropy = log(93, 2)
    max_retries = 100
    min_entropy = { 'KIT': 55, 'KITSCC': 55, 'SCC': 80 };
    max_entropy = 120

    @staticmethod
    def check_change():
        """called whenever a check method radio button is pressed;
        makes sure that the target entropy bits are set to a high enough
        value."""
        if (password_generator.entropy.get() < password_generator.min_entropy[password_generator.check_method.get()]):
            password_generator.entropy.set(password_generator.min_entropy[password_generator.check_method.get()])

    @staticmethod
    def update_entropy(entropy):
        password_generator.equivalent_length_text.set('Equiv random string: ' + str(int(int(entropy)/password_generator.per_char_entropy)) + ' chars')

    @staticmethod
    def initialize(root):
        """Initialize the entire generator UI."""
        password_generator.generator = PasswordGenerator()
        password_generator.check_method = StringVar()
        password_generator.check_method.set('KITSCC')
        password_generator.use_extended_separator_set = IntVar()
        password_generator.use_extended_separator_set.set(0)
        password_generator.equivalent_length_text = StringVar()

        # Define the layout elements
        password_generator.entropy = Scale(root, from_=password_generator.min_entropy[min(key for key in password_generator.min_entropy)], to=password_generator.max_entropy, label='Target Entropy:', orient=HORIZONTAL, command=password_generator.update_entropy)
        password_generator.entropy.set(80)
        password_generator.equivalent_length = Label(root, textvar=password_generator.equivalent_length_text)
        password_generator.equivalent_length_text.set('Equiv random string: ' + str(int(password_generator.entropy.get()/password_generator.per_char_entropy)) + ' chars')
        password_generator.button_KIT =    Radiobutton(root, text='Enforce KIT rules',                 command=password_generator.check_change, variable=password_generator.check_method, value='KIT'   )
        password_generator.button_KITSCC = Radiobutton(root, text='Enforce KIT rules/check SCC rules', command=password_generator.check_change, variable=password_generator.check_method, value='KITSCC')
        password_generator.button_SCC =    Radiobutton(root, text='Enforce SCC rules',                 command=password_generator.check_change, variable=password_generator.check_method, value='SCC'   )
        password_generator.button_extended = Checkbutton(root, text='Use extended separator set', variable=password_generator.use_extended_separator_set)
        password_generator.button_generate = Button(root, text='Generate Passwords', command=password_generator.generate)
        password_generator.output = Text(root, height=7, width=password_generator.generator.get_max_length(password_generator.max_entropy) + 4, background='black')
        password_generator.output.tag_config('error', foreground='red', justify='center')
        password_generator.output.tag_config('warning', foreground='yellow')
        password_generator.output.tag_config('okay', foreground='green')

        # Define the layout
        password_generator.entropy.grid(column=0, row=0, rowspan=3)
        password_generator.equivalent_length.grid(column=0, row=3)
        password_generator.button_KIT.grid(column=1, row=0, sticky=W)
        password_generator.button_KITSCC.grid(column=1, row=1, sticky=W)
        password_generator.button_SCC.grid(column=1, row=2, sticky=W)
        password_generator.button_extended.grid(column=1, row=3, sticky=W)
        Label(root).grid(column=0, row=4) # Add spacing
        password_generator.button_generate.grid(column=0, row=5, columnspan=2)
        Label(root).grid(column=0, row=6) # Add spacing
        password_generator.output.grid(column=0, row=7, columnspan=2)

    @staticmethod
    def generate():
        password_generator.output.delete(1.0, END)
        retries = 0
        entropy = password_generator.entropy.get()
        check_method = password_generator.check_method.get()
        use_extended_separator_set = not password_generator.use_extended_separator_set.get() == 0
        for item in range(5):
            kit_check = 1
            scc_check = 1
            while ((kit_check > 0) or ((scc_check > 0) and (check_method == 'SCC'))):
                password = password_generator.generator.generate(randomBits=entropy, use_extended=use_extended_separator_set)
                kit_check = KITPasswordChecker.check(password)
                scc_check = SCCPasswordChecker.check(password)
                retries += 1
                if (retries >= password_generator.max_retries):
                    password_generator.output.insert(END, '\nDid not find a password that satisfies the requested\nruleset within a reasonable time; giving up.\nTry increasing the target entropy to ' + str(password_generator.min_entropy[check_method]) + ' bits or higher.', 'error')
                    break
            if (retries >= password_generator.max_retries):
                break
            if ((scc_check == 0) or (check_method == 'KIT')):
                password_generator.output.insert(END, '\n  ' + password, 'okay')
            else:
                password_generator.output.insert(END, '\n  ' + password, 'warning')


def main():
    root = Tk()
    root.title('Password Generator')
    password_generator.initialize(root)
    root.mainloop()

if __name__ == "__main__":
    main()
